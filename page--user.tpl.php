<nav role="navigation" class="navbar navbar-fixed-top navbar-inverse">
     <div class="navbar-header">
  	   <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
  	  <button type='button' class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" class="navbar-btn">
	  		<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  	  </button>
      
  	   <?php if(!empty($logo)): ?>
    		 <a class="navbar-brand" href="/dashboard" title="<?php print t('Home'); ?>">
    		  	<img src="<?php print $logo; ?>"  
							style="width:50px; height : auto;" 
							alt="<?php print t('Home'); ?>"/>
						</img>
    		 </a>
  	   <?php endif; ?>
  	   <?php if ($site_name || $site_slogan): ?>
      	 	<a href="/dashboard" title="<?php print t('Home'); ?>" 
					 	class="navbar-brand syclops-navbar-brand"><?php print $_SERVER['HTTP_HOST']; ?>
				 	</a>
       	<?php if (!empty($title)): ?> 
						<a class="navbar-brand syclops-navbar-brand"><i class="icon-angle-right"></i>&nbsp;<?php print $title; ?></a>
				<?php endif; ?>
			<?php endif; ?>
			</div>
        
      	<?php if (!empty($primary_nav) || empty($secondary_nav) || !empty($page['navigation'])): ?>
            <div class="navbar-collapse collapse navbar-ex1-collapse"> 
              <nav class="nav navbar-nav navbar-right col-xs-6 col-sm-6 col-md-6">

                <?php if (!empty($primary_nav)): ?>
                    <?php print render($primary_nav); ?> 
                <?php endif; ?> 

                <?php if (!empty($secondary_nav)): ?>
                    <?php #print render($secondary_nav); ?> 
                <?php endif; ?> 

 								<?php if (!empty($page['navigation'])): ?>
                    <?php print render($page['navigation']); ?>
                <?php endif; ?>
                <?php if(user_is_logged_in()) {
                      global $user;
                       $userfields = user_load($user->uid);
                       syclops_ui_create_custom_secondary_menu($userfields); 
                      ?>           
                <?php
                      print '<ul class="nav navbar-nav  pull-right">';
                      if(in_array('site admin',$user->roles)||($user->uid==1)) {
                        print '<li><a href="/admin/index"><i class="icon-cog"></i>&nbsp;Administer</a></li>';
                      }
                        print '</ul>';
                      } else {
												if(preg_match('/^user$/', request_path())) {
                      		drupal_goto('user/login');   
												}
                    }?>
              </nav>
        		</div>
       <?php endif; ?>
</nav>

<div class="container">
	<div class="row">
		 <?php if ($page['highlighted']): ?>
     <section class="col-md-12 col-lg-12">
      	<div><?php print $messages; ?></div>
			</section>
   	<?php endif; ?>
  </div>
  <div class="row">
    <?php if ($page['sidebar_first']): ?>
      <aside class="col-md-4 col-lg-4" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>  
	  <section class="<?php print _syclops_content_span($columns); ?>" id="main-content">
      <?php #if ($breadcrumb): print $breadcrumb; endif;?>
			<?php if(!empty($page['highlighted'])): ?>
          <div class="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>

			<?php if($title): ?>
          <?php if(strpos(current_path(), 'dashboard') !== false):  ?>
            <h2 class="syclops-dashboard-header"><?php print $title; ?></h2>
          <?php endif; ?>
        <?php endif; ?>
      <?php if ($page['help']): ?> 
        <div class="well-sm"><?php print render($page['help']); ?></div>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <!-- <ul class="action-links"><?php #print render($action_links); ?></ul> -->
      <?php endif; ?>
      	<?php print render($page['content']); ?>
	  </section>

    <?php if ($page['sidebar_second']): ?>
      <aside class="col-md-4 col-lg-4" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>
   </div>
  </div>
  <footer class="footer container">
    <?php print render($page['footer']); ?>
  </footer>


	

