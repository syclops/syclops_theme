/**
 * @file
 * Main javascript implemetation for this theme.
 */
(function($) {

	if(typeof $.Syclops === "undefined") {
		$.Syclops = {};
	}

	$.Syclops.startPlayBack = function() {
    document.getElementById("session-playback").contentWindow.startRealtimePlayback();
		$('#playback-controll-panel').hide();
	};
	
	$.Syclops.hidePlayBackPanel = function() {
		 $('#playback-controll-panel').hide();		
  };

	$.Syclops.pausePlayBack = function() {
		$('#playback-controll-panel').show();
 	};

	// Extending Drupal.behaviours object.
	Drupal.behaviors.screenResize = {
		attach:function(context,settings) {			
			var documentHeight = null;
			var documentWidth = null;
			var webSSHTerminal = $('#webssh-terminal');			
			// Checking if webssh-terminal element exists in DOM.
			if(webSSHTerminal.is('*')) {
				var webSSHTerminalHeight = null;
				var webSSHTerminalResize = function() {
					// Getting document height.
               documentHeight = $(document).height();
               // Setting height.
               webSSHTerminalHeight = documentHeight-100;
               webSSHTerminal.css('min-height', webSSHTerminalHeight+'px');
							 //webSSHTerminal.css('max-height', 'auto');
				}
            // Window resize event handler.
            $(window).resize(function(event) {
					webSSHTerminalResize();
            });
            // Window on load event handler.
            $(window).load(function(event) {
					webSSHTerminalResize();
            });								
			}
			var sessionPlayback = $('#session-playback');
         // Checking if session-playback element exists in DOM.
         if(sessionPlayback.is('*')) {
            var sessionPlaybackHeight = null;
            var sessionPlaybackResize = function() {
						var container = $('#main-content');
             // Setting height.
					  //sessionPlaybackHeight = sessionPlayback.width()*0.60;					
						//sessionPlaybackHeight = sessionPlayback[0].scrollHeight + 10;
						sessionPlaybackHeight = container.width() * 0.60;
            sessionPlayback.css('min-height',sessionPlaybackHeight+'px');
            }
            // Window resize event handler.
            $(window).resize(function(event) {
               sessionPlaybackResize();
							 var fullscreenElement = document.fullscreenEnabled || document.mozFullScreenElement || document.webkitFullscreenElement;
							 if(!fullscreenElement) {
							 		$('#session-playback').contents().find('#gateone').css({
                     height : "auto",
                  	});
								}
            });
            // Window on load event handler.
            $(window).load(function(event) {
               sessionPlaybackResize();
            });
         }
			var layout = $('.layout');
         // Checking if layout element exists in DOM.
         if(layout.is('*')) {
				var layoutResize = function() {
               // Getting document width.
					documentWidth = $(document).width();
               // Checking document width.
               if(documentWidth<='767') {
						$(layout[0].nodeName+' .block').removeClass('pull-left pull-right');
               }										
				}
				// Window resize event handler.
            $(window).resize(function(event) {
					layoutResize();
            });
				// Window on load event handler.
            $(window).load(function(event) {
					layoutResize();
            });
			}
		}
	};
    
   // Extending Drupal.behaviours object.
   Drupal.behaviors.toogleToolbar = {
      attach:function(context,settings) {
        	var documentWidth = null;               
         var toggleToolbar = $('.toolbar .toolbar-nav-collapse');
			var navbarButton = $('.toolbar a.btn-navbar');
         // Checking if .toolbar-nav-collapse element exists in DOM.
         if(toggleToolbar.is('*')) {
            var toolbarResponsive = function() {
               // Getting document wdith.
               documentWidth = $(document).width();
					// Checking if navbar button is visible.
					if(navbarButton.css('display')!='none') {						
						toggleToolbar.css('height','0px');
						navbarButton.addClass('collapsed');					
					} else if(documentWidth>'767') {
						toggleToolbar.css('height','auto');
						navbarButton.removeClass('collapsed');											
					}						
            }
            // Window resize event handler.
            $(window).resize(function(event) {
               toolbarResponsive();
            });
            // Window on load event handler.
            $(window).load(function(event) {
               toolbarResponsive();
            });
         }						
		}
	};

 Drupal.behaviors.changeUploadKeyButtonPosition = {
   attach: function(context,settings) {
     
    if($('#new-private-key-wrapper', context)[0] && !$('.new-private-key-file-upload', context)[0]) {
      $('#new-private-key-wrapper', context).
      appendTo(".group-remote-system-details > .fieldset-wrapper");
      $('#edit-actions', context).addClass('col-md-12 pull-left');
     }

     if($('.new-private-key-file-upload', context)[0]) {
      $('.new-private-key-file-upload', context).
      appendTo(".group-remote-system-details > .fieldset-wrapper");
      $('#edit-actions', context).addClass('col-md-12 pull-left');

      if($('#new-private-key-wrapper', context)[0]) {
        $('#new-private-key-wrapper', context).remove();
      }

      if($('#edit-og-group-ref', context)[0]) {
        $('#edit-og-group-ref', context).hide();
      }

     }

   }
 };
 
 Drupal.behaviors.changeTerminalWidth = {
      attach:function(context,settings) {
       var path = $(location).attr("pathname");
       var pattern = new RegExp(/^\/webssh\/[0-9]*$/i);
       if(pattern.test(path)) {
        var container = $('div.container');
				container.removeClass().addClass('terminal');
       }
      }
    };

	 Drupal.behaviors.changeSummaryBlockPosition = {
      attach:function(context,settings) {
			var showMenu = function() {
				if($('.syclops-highlighted-menus li').hasClass('hidden-sm')) {
					$('.syclops-highlighted-menus li').removeClass('hidden-sm hidden-xs');
				} else {
					$('.syclops-highlighted-menus li').addClass('hidden-sm hidden-xs');
					$('.syclops-highlighted-menus .action-menu').removeClass('hidden-sm hidden-xs');
				}
			}
			var addSummaryBlock = function() {
				var pageWidth = $(window).width();
				if (pageWidth < 767) {
					if($('.syclops-summary-block-title', context)[0]) 
					{	
						 var path = $(location).attr("pathname");
       			 var pattern = new RegExp(/^\/user(s)?\/.*$/i);

						 var session = new RegExp(/^\/(.)*\/session\/[0-9]*$/i);
						 if(pattern.test(path)) {
							var summaryBlock = $('#block-views-syclops-user-block-3', context).parent().addClass('hidden-sm hidden-xs');
							var summaryBlockClone = $('#block-views-syclops-user-block-3', context).parent().html();
						 } else if(session.test(path)) {
							 var summaryBlock = $('#block-views-syclops-session-block-4', context).addClass('hidden-sm hidden-xs');
               var summaryBlockClone = $('#block-views-syclops-session-block-4', context).parent().html();

						 }

						else {
							var summaryBlock = $('.syclops-summary-block-title', context).parent();
  		          summaryBlock.parent().addClass("hidden-sm hidden-xs");
							var summaryBlockClone = $('.syclops-summary-block-title', context).parent().parent().parent().html();
						 }

						if(! $('.syclops-summary')[0]) {
							$('.highlighted .block').css({"border-bottom" : "0"});
							$('.highlighted').after(summaryBlockClone);
							var block = $('.highlighted').next();
							block.addClass("syclops-summary");
							$(".syclops-summary").removeClass('hidden-sm hidden-xs');
							$('#block-views-syclops-grant-block-3').addClass('hidden-sm hidden-xs hidden-md hidden-lg');
							$('#block-views-syclops-connection-block-3').addClass('hidden-sm hidden-xs hidden-md hidden-lg');
							$('#block-views-syclops-connection-block-1').addClass('hidden-sm hidden-xs hidden-md hidden-lg');
							$('#block-views-syclops-project-block-1').addClass('hidden-sm hidden-xs hidden-md hidden-lg');
							$('#block-views-syclops-session-block-5').addClass('hidden-sm hidden-xs hidden-md hidden-lg');
							$(".syclops-highlighted-menus a").removeClass("text-center");
							$(".syclops-highlighted-menus li").addClass("syclops-highlighted-menus-min");
							$('.syclops-highlighted-menus li').addClass('hidden-sm hidden-xs');
							$('.syclops-highlighted-menus').addClass('action-menu-mini');
              $('.syclops-highlighted-menus .action-menu').removeClass('hidden-sm hidden-xs');
						} 
							$(".syclops-summary").removeClass('hidden-sm hidden-xs');
						}  else {
              $(".syclops-highlighted-menus a").removeClass("text-center");
              $(".syclops-highlighted-menus li").addClass("syclops-highlighted-menus-min");
							$('.syclops-highlighted-menus li').addClass('hidden-sm hidden-xs');
							$('.syclops-highlighted-menus .action-menu').removeClass('hidden-sm hidden-xs'); 
            }
					$('.action-menu').click(showMenu);
				}

				if (pageWidth > 767) {
					$('.highlighted .block').css({"border-bottom" : "3px solid #bbb"});
					$(".syclops-summary").remove();
					$(".syclops-highlighted-menus a").addClass("text-center");
          $(".syclops-highlighted-menus li").removeClass("syclops-highlighted-menus-min");
					$('.syclops-highlighted-menus').removeClass('action-menu-mini');
				}

			 if (pageWidth < 992) {
          /*$('.syclops-highlighted-menus span').css({"font-size" : "68%"});*/
			
        }


			};

			addSummaryBlock();
			$(window).resize(function(){ addSummaryBlock() });
 	}
 };

	Drupal.behaviors.showPlaybackController = {
		attach : function(context, settings) {
			var startPlay = function() {
				$.Syclops.startPlayBack();
			};

			if($("#session-playback", context)[0]) {
					$(".play").bind("click", startPlay);
			}
		}
	};
	
	Drupal.behaviors.getPlaybackContainer = {
      attach:function(context, settings) {
				/*var container = '<div class="btn-group pull-right">";
					<div class="btn btn-sm btn-danger full-screen-mode" title="Toggle Fullscreen Mode">
						<span class="glyphicon glyphicon-fullscreen"></span>
					</div>
					<a href="/session/62/download" class="btn btn-primary btn-sm" title="Download Playback"><i class="icon-download-alt"></i></a>
				</div>';*/			

			var path = $(location).attr("pathname");
			var session = new RegExp(/^\/(.)*\/session\/[0-9]*$/i);
			if(session.test(path)) {
			var id= path.split("/");
			var download = '<a href="/session/'+id["3"]+'/download" class="btn btn-primary btn-sm" title="Download Playback"><i class="icon-download-alt"></i></a>';
			} else {
					var download = "";
			}
       	if($("#block-views-syclops-session-block-2", context)[0]) {
          $("#block-views-syclops-session-block-2 h3", context)
            .replaceWith( "<h3 class='block-title'>Session Playback <span class='btn-group pull-right'><span class='btn btn-sm btn-danger full-screen-mode' title='Toggle Fullscreen Mode'><span class='glyphicon glyphicon-fullscreen full-screen-mode'></span></span>"+download+"</span></h3>");
					var fullScreen =  function() {						
							 var fullscreenElement = document.fullscreenEnabled || document.mozFullScreenElement || document.webkitFullscreenElement;
							 if(!fullscreenElement) {
									//$(".full-screen-mode").removeClass("full-screen-mode").addClass("exit-full-screen");
							 		//$(".full-screen-mode").unbind("click", fullScreen);

									/*$('#session-playback').contents().find('#gateone').css({
    								 height : "650px",
									});
									*/
							 		//var element = document.getElementById('block-views-syclops-session-block-2');
									var element = document.getElementById('session-playback');
									if(element.requestFullscreen) {
    									element.requestFullscreen();
  								} else if(element.mozRequestFullScreen) {
    								element.mozRequestFullScreen();
  								} else if(element.webkitRequestFullscreen) {
    								element.webkitRequestFullscreen();
  								} else if(element.msRequestFullscreen) {
    								element.msRequestFullscreen();
  								}

									/*$('#session-playback').contents().find('#gateone').css({
                     height : "93%"
                  });*/
									//$(".exit-full-screen", context).bind("click", exitFullScreen);
								}
					};


					var exitFullScreen = function() {
						var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;
						if(fullscreenEnabled) {
								$(".exit-full-screen").removeClass("exit-full-screen").addClass("full-screen-mode");
								$(".exit-full-screen").unbind("click",  exitFullScreen);
								$(".full-screen-mode", context).bind("click",fullScreen);
								$('#session-playback').contents().find('#gateone').css({
                     height : "auto",
                  });
									//var element = document.getElementById('block-views-syclops-session-block-2');
									var element = document.getElementById('session-playback');
									if(document.fullscreenEnabled) {
    								document.exitFullscreen();
  								} else if( document.mozFullScreenEnabled) {
    								document.mozCancelFullScreen();
  								} else if(document.webkitFullscreenEnabled) {
    								document.webkitExitFullscreen();
  								}
							}
					};
				
	
				 if($("#block-views-syclops-session-block-2 .full-screen-mode",context)[0]) {
							$(".full-screen-mode").click(function() {
								$(".full-screen-mode", context).bind("click", fullScreen);
							});
				}
    	}
		}
  };
    
})(jQuery);
