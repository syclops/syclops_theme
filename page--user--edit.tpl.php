<nav role="navigation" class="navbar navbar-fixed-top navbar-inverse">
     <div class="navbar-header">
  	   <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
  	  <button type='button' class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" class="navbar-btn">
	  		<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  	  </button>
  	  
  	   <?php if(!empty($logo)): ?>
    		 <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
    		  	<img src="<?php print $logo; ?>"  style="width:50px; height : auto;" alt="<?php print t('Home'); ?>"/></img>
    		 </a>
  	   <?php endif; ?>

  	   <?php if ($site_name || $site_slogan): ?>
      	 	<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" 
					 	class="navbar-brand syclops-navbar-brand-first syclops-navbar-brand"><?php print $_SERVER['HTTP_HOST']; ?>
				 	</a>

        <?php $og = node_load(arg(1)); ?>
         <?php if($og): ?>
            <?php $project = node_load($og->og_group_ref['und'][0]['target_id']); ?>
            <a href="<?php print url('node/'.$og->og_group_ref['und'][0]['target_id'],array('absolute'=>TRUE)); ?>" 
                class="navbar-brand syclops-navbar-brand"><i class="icon-angle-right"></i>&nbsp;<?php print $og->title; ?>
            </a>
          <?endif; ?>
         	<?php if (!empty($title)): ?> 
           	<a class="navbar-brand syclops-navbar-brand">
              <i class="icon-angle-right"></i>&nbsp;<?php print $title; ?>
            </a>
       	 	<?php endif; ?>
          
				<?php endif; ?>
        </div>
        
      	<?php if (!empty($primary_nav) || empty($secondary_nav) || !empty($page['navigation'])): ?>
            <div class="navbar-collapse collapse navbar-ex1-collapse"> 
              <nav role="navigation" class="nav navbar-nav navbar-right">

                <?php if (!empty($primary_nav)): ?>
                    <?php print render($primary_nav); ?> 
                <?php endif; ?> 

                <?php if (!empty($secondary_nav)): ?>
                    <?php print render($secondary_nav); ?> 
                <?php endif; ?> 

 								<?php if (!empty($page['navigation'])): ?>
                    <?php print render($page['navigation']); ?>
                <?php endif; ?>

                  <?php
                     if(user_is_logged_in()) {
                      global $user;
                      $userfields = user_load($user->uid);
                      syclops_ui_create_custom_secondary_menu($userfields);
                 ?>           
                <?php
                      print '<ul class="nav navbar-nav">';
                      if(in_array('site admin',$user->roles)||($user->uid==1)) {
                        print '<li><a href="/admin/index"><i class="icon-cog"></i>&nbsp;Administer</a></li>';
                      }
                        print '</ul>';
                      } else {
                      //drupal_goto('user/login');   
                    }?>
              </nav>
        		</div>
       <?php endif; ?>
</nav>
<div class="container">
	<div class="row"> 
	  <section class="<?php print _syclops_content_span($columns); ?>" id="main-content">
      <div class="col-md-12 col-lg-12"><h2 class="syclops-page-title"><?php print("Edit Account Settings"); ?></h2></div>
      <div class="col-md-12 col-lg-12"><?php print $messages; ?></div>
		  <div class="col-md-12 col-lg-12"><?php print render($page['content']); ?></div>
		</section>
  </div>
  <footer class="footer container">
    <?php print render($page['footer']); ?>
  </footer>
</div>


	

