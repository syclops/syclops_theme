<?php


include_once(drupal_get_path('theme', 'syclops') . '/includes/syclops.inc');
include_once(drupal_get_path('theme', 'syclops') . '/includes/modules/theme.inc');
include_once(drupal_get_path('theme', 'syclops') . '/includes/modules/pager.inc');
include_once(drupal_get_path('theme', 'syclops') . '/includes/modules/form.inc');
include_once(drupal_get_path('theme', 'syclops') . '/includes/modules/admin.inc');
include_once(drupal_get_path('theme', 'syclops') . '/includes/modules/menu.inc');

// Load module include files
$modules = module_list();

foreach ($modules as $module) {
  if (is_file(drupal_get_path('theme', 'syclops') . '/includes/modules/' . str_replace('_', '-', $module) . '.inc')) {
    include_once(drupal_get_path('theme', 'syclops') . '/includes/modules/' . str_replace('_', '-', $module) . '.inc');
  }    
}

/**
 * hook_theme() 
 */
function syclops_theme() {
  return array(
    'syclops_links' => array(
      'variables' => array('links' => array(), 'attributes' => array(), 'heading' => NULL),
    ),
    'syclops_btn_dropdown' => array(
      'variables' => array('links' => array(), 'attributes' => array(), 'type' => NULL),
    ), 
		'user_pass' => array(
    	'render element' => 'form',
    	'path' => drupal_get_path('theme', 'syclops') . '/templates',
    	'template' => 'user-pass',
			'preprocess functions' => array(
      	'syclops_preprocess_user_pass'
    	),
		),
		'user_pass_reset' => array(
			'render element' => 'form',
    	'path' => drupal_get_path('theme', 'syclops') . '/templates',
    	'template' => 'user-pass',
			'preprocess functions' => array(
        'syclops_preprocess_user_pass'
      ),
    ),	
  );
}

/*
 * Theme helper function to override default user_pass 
 * and user_pass_rest form element style.
 */
function syclops_preprocess_user_pass(&$variables) {
	
}

function syclops_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    $breadcrumbs = '<ul class="breadcrumb">';
    
    $count = count($breadcrumb) - 1;
    foreach($breadcrumb as $key => $value) {
      if($count != $key) {
        $breadcrumbs .= '<li>'.$value.'</li>'; #'<span class="divider">/</span></li>';
      }else{
        $breadcrumbs .= '<li>'.$value.'</li>';
      }
    }
    $breadcrumbs .= '</ul>';
    
    return $breadcrumbs;
  }
}

/**
 * Preprocess variables for node.tpl.php
 *
 * @see node.tpl.php
 */
function syclops_preprocess_node(&$variables) {
  if($variables['teaser'])
    $variables['classes_array'][] = 'row';
}

/**
 * Preprocess variables for block.tpl.php
 *
 * @see block.tpl.php
 */
function syclops_preprocess_block(&$variables, $hook) {
  //$variables['classes_array'][] = 'row';
  // Use a bare template for the page's main content.
  if ($variables['block_html_id'] == 'block-system-main') {
    $variables['theme_hook_suggestions'][] = 'block__no_wrapper';
  }

  $variables['title_attributes_array']['class'][] = 'block-title';
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function syclops_process_block(&$variables, $hook) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $variables['title'] = $variables['block']->subject;
}



/**
 * Preprocess variables for page.tpl.php
 *
 * @see page.tpl.php
 */
function syclops_preprocess_page(&$variables) {
  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['columns'] = 3;
  }
  elseif (!empty($variables['page']['sidebar_first'])) {
    $variables['columns'] = 2;
  }
  elseif (!empty($variables['page']['sidebar_second'])) {
    $variables['columns'] = 2;
  }
  else {
    $variables['columns'] = 1;
  }

  // if(preg_match('/^webssh\/[0-9]*$/', request_path())) {
  //   $variables['theme_hook_suggestions'][] = 'page__webssh__%';   
  // }
   
  // Our custom search because its cool :)
  $variables['search'] = FALSE;
  if(theme_get_setting('toggle_search') && module_exists('search'))
    $variables['search'] = drupal_get_form('_syclops_search_form');

  // Primary nav
  $variables['primary_nav'] = FALSE;
  if($variables['main_menu']) {
    // Build links
    $tree = menu_tree_page_data(variable_get('menu_main_links_source', 'main-menu'));
    $variables['main_menu'] = syclops_menu_navigation_links($tree);
    
    // Build list
    $variables['primary_nav'] = theme('syclops_links', array(
      'links' => $variables['main_menu'],
      'attributes' => array(
        'id' => 'main-menu',
        'class' => array('nav'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  
  // Secondary nav
  $variables['secondary_nav'] = FALSE;
  if($variables['secondary_menu']) {
    $secondary_menu = menu_load(variable_get('menu_secondary_links_source', 'user-menu'));
    
    // Build links
    $tree = menu_tree_page_data($secondary_menu['menu_name']);
    $variables['secondary_menu'] = syclops_menu_navigation_links($tree);
    
    // Build list
    $variables['secondary_nav'] = theme('syclops_btn_dropdown', array(
      'links' => $variables['secondary_menu'],
      'label' => $secondary_menu['title'],
      'type' => 'success',
      'attributes' => array(
        'id' => 'user-menu',
        'class' => array('pull-right', 'navbar-btn'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  
  // Replace tabs with dropw down version
  $variables['tabs']['#primary'] = _syclops_local_tasks($variables['tabs']['#primary']);
}

function _syclops_search_form($form, &$form_state) {
  // Get custom search form for now
  $form = search_form($form, $form_state);

  // Cleanup
  $form['#attributes']['class'][] = 'navbar-search';
  $form['#attributes']['class'][] = 'pull-left';
  $form['basic']['keys']['#title'] = '';
  $form['basic']['keys']['#attributes']['class'][] = 'search-query';
  $form['basic']['keys']['#attributes']['class'][] = 'col-md-2';
  $form['basic']['keys']['#attributes']['placeholder'] = t('Search');
  unset($form['basic']['submit']);
  unset($form['basic']['#type']);
  unset($form['basic']['#attributes']);
  $form += $form['basic'];
  unset($form['basic']);

  return $form;
}



/**
 * Preprocess variables for region.tpl.php
 *
 * @see region.tpl.php
 */
function syclops_preprocess_region(&$variables, $hook) {
  if ($variables['region'] == 'content') {
    $variables['theme_hook_suggestions'][] = 'region__no_wrapper';
  }
  
  // Me likes
  if($variables['region'] == "sidebar_first")
    $variables['classes_array'][] = '';
}

/**
 * Returns the correct span class for a region
 */
function _syclops_content_span($columns = 1) {
  $class = FALSE;
  
  switch($columns) {
    case 1:
      $class = 'col-md-12 col-lg-12';
      break;
    case 2:
      $class = 'col-md-8 col-lg-8';
      break;
    case 3:
      $class = 'col-md-4 col-lg-4';
      break;
  }
  return $class;
}
/**
 * Implements hook_page_alter().
 */
function syclops_page_alter(&$page) {
  unset($page['content']['system_main']['user_picture']);
  unset($page['content']['system_main']['field_user_fname']);
  unset($page['content']['system_main']['field_user_lname']);
  unset($page['content']['system_main']['session_limit']);
}
