<nav role="navigation" class="navbar navbar-fixed-top navbar-inverse">
     <div class="navbar-header">
  	   <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
  	  <button type='button' class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" class="navbar-btn">
	  		<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  	  </button>
  	  
  	   <?php if(!empty($logo)): ?>
    		 <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
    		  	<img src="<?php print $logo; ?>"  style="width:50px; height : auto;" alt="<?php print t('Home'); ?>"/></img>
    		 </a>
  	   <?php endif; ?>

  	   <?php if ($site_name || $site_slogan): ?>
      	 	<a href="/dashboard" title="<?php print t('Home'); ?>" 
					 	class="navbar-brand syclops-navbar-brand"><?php print $_SERVER['HTTP_HOST']; ?>
				 	</a>

         	<?php if (!empty($title)): ?> 
           	<a class="navbar-brand syclops-navbar-brand"><i class="icon-angle-right"></i>&nbsp;<?php print $title; ?></a>
       	 	<?php endif; ?>
				<?php endif; ?>
				</div>
        
      	<?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
            <div class="navbar-collapse collapse navbar-ex1-collapse"> 
              <nav role="navigation" class="nav navbar-nav navbar-right">

                <?php if (!empty($primary_nav)): ?>
                    <?php print render($primary_nav); ?> 
                <?php endif; ?> 

                <?php if (!empty($secondary_nav)): ?>
                    <?php print render($secondary_nav); ?> 
                <?php endif; ?> 

 								<?php if (!empty($page['navigation'])): ?>
                    <?php print render($page['navigation']); ?>
                <?php endif; ?>

                 <?php
                     if(user_is_logged_in()) {
                      global $user;
                     	print '<ul class="nav navbar-nav">';
                			print '<li><a><i class="icon-signin"></i>&nbsp;Logged in: '.$user->name.'</a></li>';
                			if(in_array('site admin',$user->roles)||($user->uid==1)) {
                  			print '<li><a href="/admin/index"><i class="icon-cog"></i>&nbsp;Administer</a></li>';
                			}
                			print '</ul>';
                   }?>
              </nav>
        		</div>
       <?php endif; ?>
</nav>
<div class="container" style="padding-top : 2em;">
	<div class="row"> 
    <section class="col-md-12 col-lg-12">
      	<div><?php print $messages; ?></div>
		</section>
	  <section class="<?php print _syclops_content_span($columns); ?>" id="main-content">
				<div class="col-md-6 col-lg-6 jumbotron"> <?php print drupal_render_children($form) ?></div>
				<div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 visible-lg visible-md img-rounded" style="padding: 70px 40px;">
					<a href="http://syclops.io" target="_blank"><center><img class="img-responsive" src="/<?php print drupal_get_path('theme','syclops'); ?>/logo.png"></img></center></a>
				</div>
	  </section>
  </div>
  <footer class="footer container">
    <?php print render($page['footer']); ?>
  </footer>
</div>


	

