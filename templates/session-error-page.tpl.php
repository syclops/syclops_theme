
	<div class="page-header text-center">
		<?php if($error_title): ?>
			<h1 class="page-title"><?php  print ucfirst($error_title); ?></h1>
		<?php else: ?>
			<h1>Unknown Error</h1>
		<?php endif; ?>
	</div>
	<h3 class="text-center">	
		An error has occurred. Please contact the administrator(s) for more assistance and provide the error details.
	</h3>	
		<div class="well col-md-10 col-lg-10 col-lg-offset-1 col-md-offset-1">
		<h2 class="text-danger">Error Details</h2>
		<p>
		<?php if($error_code): ?>
				<h4>Error Code: <?php print $error_code; ?></h4>
		<?php endif; ?>

  <?php if($error_message): ?>
				<p><em><?php print $error_message; ?></em></p>
		<?php endif; ?>

		<?php if($error_details): ?>
				<h4> Error Dump</h4>
				<p><?php print $error_details; ?></p>
		<?php endif; ?>
		</p>
		</div>

	

