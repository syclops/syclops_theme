<nav role="navigation" class="navbar navbar-fixed-top navbar-inverse">
     <div class="navbar-header">
  	   <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
  	  <button type='button' class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" class="navbar-btn">
	  		<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  	  </button>

  	   <?php if(!empty($logo)): ?>
    		 <a class="navbar-brand" href="/dashboard" title="<?php print t('Home'); ?>">
    		  	<img src="<?php print $logo; ?>"  
							style="width:50px; height : auto;" 
							alt="<?php print t('Home'); ?>"/>
						</img>
    		 </a>
  	   <?php endif; ?>

  	   <?php if ($site_name || $site_slogan): ?>
      	 	<a href="/dashboard" title="<?php print t('Home'); ?>" 
					 	class="navbar-brand syclops-navbar-brand-first syclops-navbar-brand"><?php print $_SERVER['HTTP_HOST']; ?>
				 	</a>
          <?php
							if (preg_match('/manage\/project\/[0-9]*\/members/', request_path())) {
						    $title = "Manage Members";
  						}
  						if(preg_match('/manage\/project\/[0-9]*\/manager/', request_path())) {
    						$title = "Change Manager";
  						}
           ?>
           <?php if(preg_match('/manage\/(archives|project)\/[0-9]*/', request_path())): ?>
							<?php $og = node_load(arg(2)); ?>
      		    	<a href="<?php print url('node/'.$og->nid,array('absolute'=>TRUE)); ?>" 
						    	class="navbar-brand syclops-navbar-brand hidden-sm hidden-xs"><i class="icon-angle-right"></i>&nbsp;&nbsp;<?php print $og->title; ?>
					    	</a>
         	<?php endif; ?>

				  <?php if(preg_match('/[a-z0-9]\/(grant|connection|session|(^[.]*\/grant\?[.]*$))\/[0-9]*/', request_path())): ?>
						<?php $og = node_load(arg(1)); ?>
							<?php $project = node_load($og->og_group_ref['und'][0]['target_id']); ?>
              <a href="<?php print url('node/'.$og->og_group_ref['und'][0]['target_id'],array('absolute'=>TRUE)); ?>" 
                class="navbar-brand syclops-navbar-brand hidden-sm hidden-xs"><i class="icon-angle-right"></i>&nbsp;&nbsp;<?php print $project->title; ?>
              </a>
							<?php elseif(preg_match('/[.]*\/(grant|ssh-config)[?.]*$/', request_path())): ?>
              <?php $project = node_load($_REQUEST['og_group_ref']); ?>
              <a href="<?php print url('node/'.$_REQUEST['og_group_ref'],array('absolute'=>TRUE)); ?>" 
                class="navbar-brand syclops-navbar-brand hidden-sm hidden-xs"><i class="icon-angle-right"></i>&nbsp;&nbsp;<?php print $project->title; ?>
              </a>
          <?php endif; ?>

         	<?php if (!empty($title)): ?> 
						 <?php if(strpos(current_path(), 'dashboard') !== false): ?>
           		<a class="navbar-brand syclops-navbar-brand hidden-sm hidden-xs"><i class="icon-angle-right"></i>&nbsp;<?php print "Dashboard"; ?></a>
						 <?php else: ?>
							<a class="navbar-brand syclops-navbar-brand hidden-sm hidden-xs"><i class="icon-angle-right"></i>&nbsp;<?php print $title; ?></a>
						 <?php endif; ?>
       	 	<?php endif; ?>
				<?php endif; ?>
				</div>
        
      	<?php if (!empty($primary_nav) || empty($secondary_nav) || !empty($page['navigation'])): ?>
            <div class="navbar-collapse collapse navbar-ex1-collapse"> 
              <nav class="nav navbar-nav navbar-right col-xs-6 col-sm-6 col-md-4">

                <?php if (!empty($primary_nav)): ?>
                    <?php print render($primary_nav); ?> 
                <?php endif; ?> 

                <?php if (!empty($secondary_nav)): ?>
                    <?php #print render($secondary_nav); ?> 
                <?php endif; ?> 

 								<?php if (!empty($page['navigation'])): ?>
                    <?php print render($page['navigation']); ?>
                <?php endif; ?>

                 <?php
                     if(user_is_logged_in()) {
                      global $user;
                      $userfields = user_load($user->uid);
                      syclops_ui_create_custom_secondary_menu($userfields);
                 ?>    	      
                <?php
                      print '<ul class="nav navbar-nav pull-right">';
                			if(in_array('site admin',$user->roles)||($user->uid==1)) {
                  			print '<li><a href="/admin/index"><i class="icon-cog"></i>&nbsp;Administer</a></li>';
                			}
                				print '</ul>';
                   		} else {
											//drupal_goto('user/login');	 
										}?>
              </nav>
        		</div>
       <?php endif; ?>
</nav>

<div class="container">
	<div class="row">
     <section class="col-md-12 col-lg-12">
          <?php if($title): ?>
            <?php if(preg_match('/^([a-z0-9]*|dashboard|\S)$/', request_path())):  ?>
            <?php elseif(preg_match('/[a-z0-9]\/connection\/[0-9]*/', request_path())):  ?>
						<?php elseif(preg_match('/^[a-z0-9]*\/(grant|session)\/[0-9]*$/', request_path())):  ?>
						<?php elseif(preg_match('/^webssh\/[0-9]*$/', request_path())):  ?>
						<?php elseif(preg_match('/^system\/tfa\/[0-9]+\/.+$/', request_path())):  print '<h2>'.$title.'</h2><hr>'; ?>
						<?php else: ?>
							<?#php print $title; ?>
            <?php endif; ?>
          <?php endif; ?>
      </section>

		<?php if ($page['highlighted']): ?>
     		<section class="col-md-12 col-lg-12">
      		<div><?php print $messages; ?></div>
				</section>
   	<?php endif; ?>

  </div>
  <div class="row">
  <?php 
    $local_flag = flag_get_flag('local_archive');
    $parent_flag = flag_get_flag('parent_archive');
     if(!empty($node) && $local_flag->is_flagged($node->nid)):
      if(preg_match('/^[a-z0-9\-]*\/(grant|connection)\/[0-9]*$/', request_path())):
        $message = block_load('block',4); ?>
        <?php if(!empty($page['highlighted'])): ?>
            <div class="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <?php print drupal_render(_block_get_renderable_array(_block_render_blocks(array($message))));
      endif;
    else:
   ?>
   
    <?php if ($page['sidebar_first']): ?>
      <aside class="col-md-4" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>  
	  
	  <section class="<?php print _syclops_content_span($columns); ?>" id="main-content">
      <?php #if ($breadcrumb): print $breadcrumb; endif;?>
			<?php if(!empty($page['highlighted'])): ?>
          <div class="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>

			<?php if($title): ?>
          <?php if(strpos(current_path(), 'dashboard') !== false):  ?>
            <h2 class="syclops-dashboard-header"><?php print $title; ?></h2>
          <?php endif; ?>
        <?php endif; ?>
      <?php #print $messages; ?>

      <?#php if($tabs && _syclops_ui_is_admin()): 
        #print render($tabs); 
        #endif;?>

      <?php if($tabs): ?>
				<?php if(strpos(current_path(), 'manage/archives/') !== false)print render($tabs); ?>
      <?php endif; ?>

     <?php if($tabs): ?>
        <?php if(strpos(current_path(), 'manage/project/') !== false)print render($tabs); ?>
      <?php endif; ?>


      <?php if ($page['help']): ?> 
        <div class="well-sm"><?php print render($page['help']); ?></div>
      <?php endif; ?>

    
      <?php if ($action_links): ?>
        <!-- <ul class="action-links"><?php #print render($action_links); ?></ul> -->
      <?php endif; ?>

      <?php print render($page['content']); ?>
	  </section>

    <?php if ($page['sidebar_second']): ?>
      <aside class="col-md-4 col-lg-4" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>
   </div>
 <?php endif; ?>
  </div>
  <footer class="footer container">
    <?php print render($page['footer']); ?>
  </footer>


	

